﻿using UnityEngine;
using System.Collections;

public class PlayerController2 : MonoBehaviour {

    public GameObject Bolt;
    public float Firerate = 0.5f;
    public Transform GunBarrel;
    public Transform Cannon;
    public float RotationSpeed = 5f;
    //public bool FireMode = true;
    public Transform CrosshairOverlay;
    

    public Camera camera;
    public GameObject crosshair;

    public enum FireModes { Classic, Crosshair}

    public FireModes FireMode;

    private float _nextFire = 0;
    private Vector3 RotateVector = new Vector3();

    void Start()
    {
        
    }

    void Update()
    {
        _fire();

        switch (FireMode)
        {
            case FireModes.Classic:
                _ClassicControl();
                break;

            case FireModes.Crosshair:
                _CrossHairControl();
                break;
        }
        
    }

    private void _fire()
    {
        if (Input.GetButton("Fire1") && Time.time > _nextFire)
        {
            _nextFire = Time.time + Firerate;
            Instantiate(Bolt, GunBarrel.position, GunBarrel.rotation);
        }
    }

    private void _ClassicControl()
    {
        RotateVector.Set(0, 0, Input.GetAxis("CannonRotation") * RotationSpeed * Time.deltaTime);

        Cannon.Rotate(RotateVector);
    }

    private void _CrossHairControl()
    {
        #region Crosshair Position
        Vector3 desiredPosition = Input.mousePosition;
        desiredPosition.z = camera.transform.position.z * -1;
        crosshair.transform.position = Camera.main.ScreenToWorldPoint(desiredPosition);
        #endregion;

        Cannon.eulerAngles = new Vector3(Cannon.rotation.x, Cannon.rotation.y, -90 + Mathf.Atan2((crosshair.transform.position.y - Cannon.position.y), (crosshair.transform.position.x - Cannon.position.x)) * Mathf.Rad2Deg);

        //Cannon.rotation = Quaternion.Euler(desiredPosition.x,desiredPosition.y,desiredPosition.z);
    }
}
