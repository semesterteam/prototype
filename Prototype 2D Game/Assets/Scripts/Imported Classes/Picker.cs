﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
//using System.Threading.Tasks;


    public class Picker<T>
    {
        private List<KeyValuePair<T, int>> _items = new List<KeyValuePair<T, int>>();
        private int _count = 0;
        private readonly Random _r = new Random(Guid.NewGuid().GetHashCode());
        private int _multiplier = 1;

        public void Add(T item, float chance)
        {
            if (chance < 0)
            {
                throw new ChanceCannotBeNegativeException("Since they dont exist, negative chances are not supported");
            }
            if (chance < 0.0001f)
                throw new PickerOverflowException("Picker can not handle that many Decimal Places");

            chance = chance * Convert.ToSingle(_multiplier);



            if (TooManyDecimalPlaces(chance))
                throw new PickerOverflowException("Too many decimal places");


            while (chance % 1f != 0)
            {
                chance *= 10f;
                _multiplier *= 10;

                for (int i = 0; i < _items.Count; i++)
                {
                    int tmpValue = _items[i].Value;
                    T tmpKey = _items[i].Key;
                    _items[i] = new KeyValuePair<T, int>(tmpKey, tmpValue * 10);

                }
                _count *= 10;

            }
            _count += Convert.ToInt32(chance);
            _items.Add(new KeyValuePair<T, int>(item, _count));

        }

        public void Add(T item, int chance)
        {
            if (chance < 0)
            {
                throw new ChanceCannotBeNegativeException("Since they dont exist, negative chances are not supported");
            }


            if (NumberTooBig(chance))
            {
                throw new PickerOverflowException("The Item's chance does not fit into the Picker anymore");
            }
            chance *= _multiplier;

            _count += chance;
            _items.Add(new KeyValuePair<T, int>(item, _count));
        }

        public T Pick()
        {
            int randomZahl = _r.Next(1, (_count + 1));

            for (int i = 0; i < _items.Count; i++)
            {
                if (randomZahl <= _items[i].Value) return _items[i].Key;
            }
            throw new UnableToPickRandomNumberException();
            
        }

        //public void WriteContent()
        //{
        //    Console.WriteLine("The following Items with Chance are enlisted in this Picker: ");

        //    Console.WriteLine($"{_items[0].Key}, {_items[0].Value}");

        //    for (int i = 1; i < _items.Count; i++)
        //    {
        //        Console.WriteLine($"{_items[i].Key}, {_items[i].Value /*-_items[i-1].Value*/}");
        //    }
        //    Console.WriteLine();
        //}

        private bool TooManyDecimalPlaces(float chance)
        {
            long tmpCount = Convert.ToInt64(_count);
            long tmpMultiplier = Convert.ToInt64(_multiplier);

            chance = chance * Convert.ToSingle(tmpMultiplier);

            while (chance % 1f != 0)
            {
                if ((tmpCount * 10 + Convert.ToInt64(chance * 10f)) >= Convert.ToInt64(int.MaxValue))
                    return true;

                chance *= 10f;
                tmpCount *= 10;
            }
            return false;
        }
        private bool NumberTooBig(int chance)
        {
            return ((long)chance * (long)_multiplier) > int.MaxValue
                    || (((long)chance * _multiplier) + _count) > int.MaxValue;
        }
    }

    [Serializable]
    public class ChanceCannotBeNegativeException : Exception
    {
        public ChanceCannotBeNegativeException() { }

        public ChanceCannotBeNegativeException(string message) : base(message)
        { }
    }
   
    [Serializable]
    internal class UnableToPickRandomNumberException : Exception
    {
        public UnableToPickRandomNumberException()
        {
        }

        public UnableToPickRandomNumberException(string message) : base(message)
        {
        }

        public UnableToPickRandomNumberException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnableToPickRandomNumberException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    internal class PickerOverflowException : Exception
    {
        public PickerOverflowException()
        {
        }

        public PickerOverflowException(string message) : base(message)
        {
        }

        public PickerOverflowException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PickerOverflowException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

