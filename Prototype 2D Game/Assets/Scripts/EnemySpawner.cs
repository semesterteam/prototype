﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Random = System.Random;
using System.Runtime.Serialization;

public class EnemySpawner : MonoBehaviour
{
    public Transform SpawnField;
    public GameObject[] Enemys;
    public int[] Chances;

    private Picker<GameObject> _EnemyPicker = new Picker<GameObject>();
    private readonly  Random _r = new Random(Guid.NewGuid().GetHashCode());
    private float _xMin;
    private float _xMax;

    void Start()
    {
        if (Enemys.Length != Chances.Length)
        {
            gameObject.SetActive(false);
            throw new EnemysAndChancesAreNotEqualException();
        }
        SpawnField = GetComponent<Transform>();
        InitPicker();
        InitSpawnRange();
        SpawnEnemy();
    }

    

    public void SpawnEnemy()
    {
        Instantiate(_EnemyPicker.Pick(), new Vector3(Convert.ToSingle(_r.Next(Convert.ToInt32(_xMin),Convert.ToInt32(_xMax))), SpawnField.localPosition.y, 0), Quaternion.AngleAxis(180f, new Vector3(0,0,1f))); // das Quaternion ist falsch
    }

    private void InitPicker()
    {
        for (int i = 0; i < Enemys.Length; i++)
        {
            _EnemyPicker.Add(Enemys[i], Chances[i]);
        }
    }

    private void InitSpawnRange()
    {
        _xMin = SpawnField.localPosition.x - (SpawnField.localScale.x/2);
        _xMax = SpawnField.localPosition.x + (SpawnField.localScale.x / 2);
    }
    
}

[Serializable]
internal class EnemysAndChancesAreNotEqualException : Exception
{
    public EnemysAndChancesAreNotEqualException()
    {
    }

    public EnemysAndChancesAreNotEqualException(string message) : base(message)
    {
    }

    public EnemysAndChancesAreNotEqualException(string message, Exception innerException) : base(message, innerException)
    {
    }

    protected EnemysAndChancesAreNotEqualException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}

//[CustomEditor(typeof(EnemySpawner))]
//[CanEditMultipleObjects]
//public class EnemyArrayDisplayer : Editor
//{
//    public  void ShowArrayProperty(SerializedProperty list)
//    {
//        EditorGUILayout.PropertyField(list);

//        EditorGUI.indentLevel += 1;
//        for (int i = 0; i < list.arraySize; i++)
//        {
//            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i),
//            new GUIContent("Enemy" + (i + 1).ToString()));
//        }
//        EditorGUI.indentLevel -= 1;
//    }
//}
