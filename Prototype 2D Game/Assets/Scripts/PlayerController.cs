﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class PlayerController : MonoBehaviour
{

    public float MovementSpeed = 35f;
    public float PlayerMovementClampY;
    public float PlayerMovementClampX;

    private Transform _playerTransform;
    private Vector3 _movement = new Vector3();
    

    void Start()
    {
        _playerTransform = GetComponent<Transform>();
        //GunBarrel = GetComponentInChildren<Transform>();
        //MovementSpeed = 10f;
    }

    void Update()
    {
        _PlayerMovementLimit();
    }

    void FixedUpdate()
    {
        _movement.Set(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        _playerTransform.position += _movement*MovementSpeed*Time.deltaTime;
    }

    private void _PlayerMovementLimit()
    {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp(pos.x, 0+PlayerMovementClampX, 1-PlayerMovementClampX);
        pos.y = Mathf.Clamp(pos.y, 0+PlayerMovementClampY, 1-PlayerMovementClampY);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }
}
