﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{

    public float MovementSpeed = 50f;

    private Rigidbody2D _rb2d;
    private Transform _transform;

    void Awake()
    {
        _rb2d = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();
    }

    void Start()
    {
        _rb2d.velocity = _transform.up*MovementSpeed;
    }

}
